package com.rabobank.kramchanynov.model;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.List;

@RunWith(AndroidJUnit4.class)
public class FileLoaderTest {

    private final String NON_EXISTENT_FILENAME = "non_existent_file.csv";
    private final String VALID_NON_CSV_FILENAME = "issues.txt";
    private final String VALID_CSV_FILENAME = "issues.csv";
    private final String INVALID_CSV_FILENAME = "issues_invalid.csv";

    private final Record VALID_FILE_HEADER = new Record("First name","Sur name","Issue count","Date of birth");
    private final Record VALID_FILE_RECORD_ONE = new Record("Theo","Jansen", "5","1978-01-02T00:00:00");
    private final Record VALID_FILE_RECORD_TWO = new Record("Fiona","de Vries", "7","1950-11-12T00:00:00");
    private final Record VALID_FILE_RECORD_THREE = new Record("Petra","Boersma", "1","2001-04-20T00:00:00");

    private final Record INVALID_FILE_HEADER = new Record("First name","","Issue count","Date of birth");
    private final Record INVALID_FILE_RECORD_ONE = new Record("Theo","Jansen", "","1978-01-02T00:00:00");
    private final Record INVALID_FILE_RECORD_TWO = new Record("Petra", "","1","2001-04-20T00:00:00");

    private final Context mAppContext = InstrumentationRegistry.getTargetContext();

    @Test
    public void loadNonExistentFile() {
        FileLoader loader = new FileLoader(mAppContext, NON_EXISTENT_FILENAME);

        final List<Record> records = loader.loadInBackground();
        assertTrue(records.isEmpty());
    }

    @Test
    public void loadValidNonCSVFile() {
        FileLoader loader = new FileLoader(mAppContext, VALID_NON_CSV_FILENAME);

        final List<Record> records = loader.loadInBackground();
        assertFalse(records.isEmpty());
        assertEquals(4, records.size());

        assertEquals(VALID_FILE_HEADER, records.get(0));
        assertEquals(VALID_FILE_RECORD_ONE, records.get(1));
        assertEquals(VALID_FILE_RECORD_TWO, records.get(2));
        assertEquals(VALID_FILE_RECORD_THREE, records.get(3));
    }

    @Test
    public void loadValidCSVFile() {
        FileLoader loader = new FileLoader(mAppContext, VALID_CSV_FILENAME);

        final List<Record> records = loader.loadInBackground();
        assertFalse(records.isEmpty());
        assertEquals(4, records.size());

        assertEquals(VALID_FILE_HEADER, records.get(0));
        assertEquals(VALID_FILE_RECORD_ONE, records.get(1));
        assertEquals(VALID_FILE_RECORD_TWO, records.get(2));
        assertEquals(VALID_FILE_RECORD_THREE, records.get(3));
    }

    @Test
    public void loadInvalidCSVFile() {
        FileLoader loader = new FileLoader(mAppContext, INVALID_CSV_FILENAME);

        final List<Record> records = loader.loadInBackground();
        assertFalse(records.isEmpty());
        assertEquals(3, records.size());

        assertEquals(INVALID_FILE_HEADER, records.get(0));
        assertEquals(INVALID_FILE_RECORD_ONE, records.get(1));
        assertEquals(INVALID_FILE_RECORD_TWO, records.get(2));
    }
}
