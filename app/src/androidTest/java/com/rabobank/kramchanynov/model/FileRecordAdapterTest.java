package com.rabobank.kramchanynov.model;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.view.View;
import android.widget.TextView;

import com.rabobank.kramchanynov.rabobank.R;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;

import static com.rabobank.kramchanynov.model.FileRecordAdapter.ViewHolder.HEADER_ITEM_VIEW_TYPE;
import static com.rabobank.kramchanynov.model.FileRecordAdapter.ViewHolder.REGULAR_ITEM_VIEW_TYPE;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(AndroidJUnit4.class)
public class FileRecordAdapterTest {

    private final Record VALID_FILE_HEADER = new Record("First name","Sur name","Issue count","Date of birth");
    private final Record VALID_FILE_RECORD = new Record("Theo","Jansen", "5","1978-01-02T00:00:00");
    private final Record INVALID_FILE_RECORD = new Record("","", "","");

    @Mock
    private View mViewMock;

    private TextView mFirstNameView;
    private TextView mLastNameView;
    private TextView mIssueCountView;
    private TextView mBirthdayView;

    private final Context mAppContext = InstrumentationRegistry.getTargetContext();

    @Before
    public void setUp() {
        mViewMock = mock(View.class);
        mFirstNameView = new TextView(mAppContext);
        mLastNameView = new TextView(mAppContext);
        mIssueCountView = new TextView(mAppContext);
        mBirthdayView = new TextView(mAppContext);

        when(mViewMock.findViewById(R.id.fist_name_view)).thenReturn(mFirstNameView);
        when(mViewMock.findViewById(R.id.last_name_view)).thenReturn(mLastNameView);
        when(mViewMock.findViewById(R.id.issue_count_view)).thenReturn(mIssueCountView);
        when(mViewMock.findViewById(R.id.birthday_view)).thenReturn(mBirthdayView);
    }

    @Test
    public void displayHeader() {
        FileRecordAdapter.ViewHolder viewHolder = new FileRecordAdapter.ViewHolder(mViewMock, HEADER_ITEM_VIEW_TYPE);
        viewHolder.displayRecord(VALID_FILE_HEADER);

        assertEquals(VALID_FILE_HEADER.getFirstName(), mFirstNameView.getText());
        assertEquals(VALID_FILE_HEADER.getLastName(), mLastNameView.getText());
        assertEquals(VALID_FILE_HEADER.getIssueCount(), mIssueCountView.getText());
        assertEquals(VALID_FILE_HEADER.getDateOfBirth(), mBirthdayView.getText());
    }

    @Test
    public void displayValidRecord() {
        FileRecordAdapter.ViewHolder viewHolder = new FileRecordAdapter.ViewHolder(mViewMock, REGULAR_ITEM_VIEW_TYPE);
        viewHolder.displayRecord(VALID_FILE_RECORD);

        assertEquals(VALID_FILE_RECORD.getFirstName(), mFirstNameView.getText());
        assertEquals(VALID_FILE_RECORD.getLastName(), mLastNameView.getText());
        assertEquals(VALID_FILE_RECORD.getIssueCount(), mIssueCountView.getText());
        assertEquals("02-Jan-1978", mBirthdayView.getText());
    }

    @Test
    public void displayInvalidRecord() {
        FileRecordAdapter.ViewHolder viewHolder = new FileRecordAdapter.ViewHolder(mViewMock, REGULAR_ITEM_VIEW_TYPE);
        viewHolder.displayRecord(INVALID_FILE_RECORD);

        assertEquals("N/A", mFirstNameView.getText());
        assertEquals("N/A", mLastNameView.getText());
        assertEquals("N/A", mIssueCountView.getText());
        assertEquals("N/A", mBirthdayView.getText());
    }

    @Test
    public void displayHeaderAsRegularRecord() {
        FileRecordAdapter.ViewHolder viewHolder = new FileRecordAdapter.ViewHolder(mViewMock, REGULAR_ITEM_VIEW_TYPE);
        viewHolder.displayRecord(VALID_FILE_HEADER);

        assertEquals(VALID_FILE_HEADER.getFirstName(), mFirstNameView.getText());
        assertEquals(VALID_FILE_HEADER.getLastName(), mLastNameView.getText());
        assertEquals(VALID_FILE_HEADER.getIssueCount(), mIssueCountView.getText());
        assertEquals("N/A", mBirthdayView.getText());
    }

    @Test
    public void displayValidRecordAsHeader() {
        FileRecordAdapter.ViewHolder viewHolder = new FileRecordAdapter.ViewHolder(mViewMock, HEADER_ITEM_VIEW_TYPE);
        viewHolder.displayRecord(VALID_FILE_RECORD);

        assertEquals(VALID_FILE_RECORD.getFirstName(), mFirstNameView.getText());
        assertEquals(VALID_FILE_RECORD.getLastName(), mLastNameView.getText());
        assertEquals(VALID_FILE_RECORD.getIssueCount(), mIssueCountView.getText());
        assertEquals(VALID_FILE_RECORD.getDateOfBirth(), mBirthdayView.getText());
    }
}
