package com.rabobank.kramchanynov.model;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.opencsv.CSVReader;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(AndroidJUnit4.class)
public class AssetFileParserTest {

    private final String NON_EXISTENT_FILENAME = "non_existent_file.csv";
    private final String VALID_NON_CSV_FILENAME = "issues.txt";
    private final String VALID_CSV_FILENAME = "issues.csv";
    private final String INVALID_CSV_FILENAME = "issues_invalid.csv";

    private final String[] VALID_RAW_HEADER_RECORD = {"First name", "Sur name", "Issue count", "Date of birth"};
    private final String[] VALID_RAW_RECORD_ONE = {"Theo", "Jansen", "5", "1978-01-02T00:00:00"};
    private final String[] VALID_RAW_RECORD_TWO = {"Fiona", "de Vries", "7", "1950-11-12T00:00:00"};
    private final String[] VALID_RAW_RECORD_THREE = {"Petra", "Boersma", "1", "2001-04-20T00:00:00"};

    private final String[] INVALID_RAW_HEADER_RECORD = {"First name", "", "Issue count", "Date of birth"};
    private final String[] INVALID_RAW_RECORD_ONE = {"Theo", "Jansen", "", "1978-01-02T00:00:00"};
    private final String[] INVALID_RAW_RECORD_TWO = {"Fiona", "", "de Vries", "7", "1950-11-12Tgv00:00:00"};
    private final String[] INVALID_RAW_RECORD_THREE = {"Petra", "", "1", "2001-04-20T00:00:00"};

    private final Context mAppContext = InstrumentationRegistry.getTargetContext();

    @Test
    public void parseNonExistentFile() {
        final List<String[]> rawRecords = AssetFileParser.parse(mAppContext.getAssets(),
                NON_EXISTENT_FILENAME);
        assertTrue(rawRecords.isEmpty());
    }

    @Test
    public void parseValidNonCSVFile() {
        final List<String[]> rawRecords = AssetFileParser.parse(mAppContext.getAssets(),
                VALID_NON_CSV_FILENAME);
        assertFalse(rawRecords.isEmpty());
        assertEquals(4, rawRecords.size());

        assertEquals(4, rawRecords.get(0).length);
        assertEquals(4, rawRecords.get(1).length);
        assertEquals(4, rawRecords.get(2).length);
        assertEquals(4, rawRecords.get(3).length);

        assertArrayEquals(VALID_RAW_HEADER_RECORD, rawRecords.get(0));
        assertArrayEquals(VALID_RAW_RECORD_ONE, rawRecords.get(1));
        assertArrayEquals(VALID_RAW_RECORD_TWO, rawRecords.get(2));
        assertArrayEquals(VALID_RAW_RECORD_THREE, rawRecords.get(3));
    }

    @Test
    public void parseValidCSVFile() {
        final List<String[]> rawRecords = AssetFileParser.parse(mAppContext.getAssets(),
                VALID_CSV_FILENAME);
        assertFalse(rawRecords.isEmpty());
        assertEquals(4, rawRecords.size());

        assertEquals(4, rawRecords.get(0).length);
        assertEquals(4, rawRecords.get(1).length);
        assertEquals(4, rawRecords.get(2).length);
        assertEquals(4, rawRecords.get(3).length);

        assertArrayEquals(VALID_RAW_HEADER_RECORD, rawRecords.get(0));
        assertArrayEquals(VALID_RAW_RECORD_ONE, rawRecords.get(1));
        assertArrayEquals(VALID_RAW_RECORD_TWO, rawRecords.get(2));
        assertArrayEquals(VALID_RAW_RECORD_THREE, rawRecords.get(3));
    }

    @Test
    public void parseInvalidCSVFile() {
        final List<String[]> rawRecords = AssetFileParser.parse(mAppContext.getAssets(),
                INVALID_CSV_FILENAME);
        assertFalse(rawRecords.isEmpty());
        assertEquals(4, rawRecords.size());

        assertEquals(4, rawRecords.get(0).length);
        assertEquals(4, rawRecords.get(1).length);
        assertEquals(5, rawRecords.get(2).length);
        assertEquals(4, rawRecords.get(3).length);

        assertArrayEquals(INVALID_RAW_HEADER_RECORD, rawRecords.get(0));
        assertArrayEquals(INVALID_RAW_RECORD_ONE, rawRecords.get(1));
        assertArrayEquals(INVALID_RAW_RECORD_TWO, rawRecords.get(2));
        assertArrayEquals(INVALID_RAW_RECORD_THREE, rawRecords.get(3));
    }

    @Test
    public void parseReadAllException() throws IOException {
        CSVReader csvReader = mock(CSVReader.class);

        when(csvReader.readAll()).thenThrow(IOException.class);

        final List<String[]> rawRecords = AssetFileParser.parse(csvReader);

        assertTrue(rawRecords.isEmpty());
    }
}
