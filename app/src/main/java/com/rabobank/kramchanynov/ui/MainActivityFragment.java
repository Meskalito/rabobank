package com.rabobank.kramchanynov.ui;

import android.app.Fragment;
import android.app.LoaderManager;
import android.content.Loader;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.rabobank.kramchanynov.model.FileLoader;
import com.rabobank.kramchanynov.model.FileRecordAdapter;
import com.rabobank.kramchanynov.model.Record;
import com.rabobank.kramchanynov.rabobank.R;

import java.util.ArrayList;
import java.util.List;

public class MainActivityFragment extends Fragment implements LoaderManager.LoaderCallbacks<List<Record>> {

    private final String FILE_NAME = "issues.csv";

    private static final int FILE_LOADER_ID = 1;

    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private FileRecordAdapter mFileRecordAdapter;
    private List<Record> mRecords = new ArrayList<>();

    public MainActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getLoaderManager().initLoader(FILE_LOADER_ID, null, this);

        mRecyclerView = getActivity().findViewById(R.id.recycler_view);

        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        mFileRecordAdapter = new FileRecordAdapter(mRecords);

        mRecyclerView.setAdapter(mFileRecordAdapter);
    }

    @Override
    public Loader<List<Record>> onCreateLoader(int id, Bundle args) {
        return new FileLoader(getActivity(), FILE_NAME);
    }

    @Override
    public void onLoadFinished(Loader<List<Record>> loader, List<Record> data) {
        mFileRecordAdapter.clearRecords();
        mFileRecordAdapter.addRecords(data);
    }

    @Override
    public void onLoaderReset(Loader<List<Record>> loader) {
        mRecords = null;
        mFileRecordAdapter.clearRecords();
    }
}
