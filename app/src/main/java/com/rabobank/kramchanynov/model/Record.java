package com.rabobank.kramchanynov.model;

public class Record {

    private final String mFirstName;
    private final String mLastName;
    private final String mIssueCount;
    private final String mDateOfBirth;

    public Record(String mFirstName, String mLastName, String mIssueCount, String mDateOfBirth) {
        this.mFirstName = mFirstName;
        this.mLastName = mLastName;
        this.mIssueCount = mIssueCount;
        this.mDateOfBirth = mDateOfBirth;
    }

    public String getFirstName() {
        return mFirstName;
    }

    public String getLastName() {
        return mLastName;
    }

    public String getIssueCount() {
        return mIssueCount;
    }

    public String getDateOfBirth() {
        return mDateOfBirth;
    }

    // Automatically generated
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Record record = (Record) o;

        if (!mFirstName.equals(record.mFirstName)) return false;
        if (!mLastName.equals(record.mLastName)) return false;
        if (!mIssueCount.equals(record.mIssueCount)) return false;
        return mDateOfBirth.equals(record.mDateOfBirth);
    }


    // Automatically generated
    @Override
    public int hashCode() {
        int result = mFirstName.hashCode();
        result = 31 * result + mLastName.hashCode();
        result = 31 * result + mIssueCount.hashCode();
        result = 31 * result + mDateOfBirth.hashCode();
        return result;
    }
}
