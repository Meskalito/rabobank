package com.rabobank.kramchanynov.model;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.rabobank.kramchanynov.rabobank.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

public class FileRecordAdapter extends RecyclerView.Adapter<FileRecordAdapter.ViewHolder> {

    private final List<Record> mRecords;

    public FileRecordAdapter(List<Record> records) {
        mRecords = records;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.record_view, parent, false);
        return new ViewHolder(view, viewType);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.displayRecord(mRecords.get(position));
    }

    @Override
    public int getItemViewType(int position) {
        return position == 0 ? ViewHolder.HEADER_ITEM_VIEW_TYPE : ViewHolder.REGULAR_ITEM_VIEW_TYPE;
    }

    @Override
    public int getItemCount() {
        return mRecords.size();
    }

    public void addRecords(List<Record> items) {
        if (mRecords != null) {
            mRecords.clear();
            mRecords.addAll(items);
        }
    }

    public void clearRecords() {
        if (mRecords != null) {
            mRecords.clear();
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        static final int HEADER_ITEM_VIEW_TYPE = 0;
        static final int REGULAR_ITEM_VIEW_TYPE = 1;

        private final SimpleDateFormat RAW_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH);
        private final SimpleDateFormat DISPLAY_DATE_FORMAT = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);
        private final String NOT_AVAILABLE = "N/A";

        private final TextView mFirstNameView;
        private final TextView mLastNameView;
        private final TextView mIssueCountView;
        private final TextView mBirthdayView;

        private final int mViewType;

        ViewHolder(View v, int viewType) {
            super(v);
            mFirstNameView = v.findViewById(R.id.fist_name_view);
            mLastNameView = v.findViewById(R.id.last_name_view);
            mIssueCountView = v.findViewById(R.id.issue_count_view);
            mBirthdayView = v.findViewById(R.id.birthday_view);

            mViewType = viewType;

            if (mViewType == HEADER_ITEM_VIEW_TYPE) {
                v.setBackgroundColor(Color.LTGRAY);
            }
        }

        public void displayRecord(final Record record) {
            if (mViewType == HEADER_ITEM_VIEW_TYPE) {
                displayHeader(record);
            } else {
                displayRegularRecord(record);
            }
        }

        private void displayHeader(final Record record) {
            mFirstNameView.setText(record.getFirstName());
            mLastNameView.setText(record.getLastName());
            mIssueCountView.setText(String.valueOf(record.getIssueCount()));
            mBirthdayView.setText(record.getDateOfBirth());
        }

        private void displayRegularRecord(final Record record) {
            mFirstNameView.setText(record.getFirstName().isEmpty() ? NOT_AVAILABLE : record.getFirstName());
            mLastNameView.setText(record.getLastName().isEmpty() ? NOT_AVAILABLE : record.getLastName());
            mIssueCountView.setText(record.getIssueCount().isEmpty() ? NOT_AVAILABLE : record.getIssueCount());

            try {
                mBirthdayView.setText(DISPLAY_DATE_FORMAT.format(RAW_DATE_FORMAT.parse(record.getDateOfBirth())));
            } catch (ParseException e) {
                mBirthdayView.setText(NOT_AVAILABLE);
            }
        }
    }
}
