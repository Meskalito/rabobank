package com.rabobank.kramchanynov.model;

import android.content.res.AssetManager;
import android.support.annotation.VisibleForTesting;
import android.util.Log;

import com.opencsv.CSVReader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.List;

class AssetFileParser {

    private static final String TAG = AssetFileParser.class.getSimpleName();

    public static List<String[]> parse(final AssetManager assetManager, final String filename) {
        List<String[]> result = Collections.emptyList();
        final InputStream inputStream;
        try {
            inputStream = assetManager.open(filename);
            final CSVReader csvReader = new CSVReader(new InputStreamReader(inputStream));
            result = parse(csvReader);
        } catch (IOException e) {
            Log.e(TAG, "I/O error occurred when reading file: " + e.getMessage());
        }
        return result;
    }

    @VisibleForTesting
    static List<String[]> parse(final CSVReader csvReader) {
        List<String[]> result = Collections.emptyList();

        try {
            try {
                result = csvReader.readAll();
            } catch (IOException e) {
                Log.e(TAG, "I/O error occurred when reading file: " + e.getMessage());
            } finally {
                if (csvReader != null) {
                    csvReader.close();
                }
            }
        } catch (IOException e) {
            Log.e(TAG, "I/O error occurred when reading file: " + e.getMessage());
        }
        return result;
    }
}
