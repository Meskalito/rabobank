package com.rabobank.kramchanynov.model;

import android.content.AsyncTaskLoader;
import android.content.Context;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class FileLoader extends AsyncTaskLoader<List<Record>> {

    private final String TAG = FileLoader.class.getSimpleName();

    private final int TOTAL_NUMBER_OF_FIELDS = 4;

    private static final int FIRST_NAME_FIELD_POSITION = 0;
    private static final int LAST_NAME_FIELD_POSITION = 1;
    private static final int ISSUE_COUNT_FIELD_POSITION = 2;
    private static final int DATE_OF_BIRTH_FIELD_POSITION = 3;

    private final String mFileName;

    private List<Record> mRecords;

    public FileLoader(Context context, final String fileName) {
        super(context);
        mFileName = fileName;
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();

        if (mRecords != null) {
            deliverResult(mRecords);
        }

        if (mRecords == null || takeContentChanged()) {
            forceLoad();
        }
    }

    @Override
    public List<Record> loadInBackground() {
        mRecords = new ArrayList<>();

        final List<String[]> rawRecords = AssetFileParser.parse(getContext().getAssets(), mFileName);

        for (String[] rawRecord : rawRecords) {
            if (rawRecord.length != TOTAL_NUMBER_OF_FIELDS) {
                Log.e(TAG, "Insufficient record format. Skip broken record");
            } else {
                mRecords.add(new Record(rawRecord[FIRST_NAME_FIELD_POSITION], rawRecord[LAST_NAME_FIELD_POSITION],
                        rawRecord[ISSUE_COUNT_FIELD_POSITION], rawRecord[DATE_OF_BIRTH_FIELD_POSITION]));
            }
        }

        return mRecords;
    }

    @Override
    public void deliverResult(List<Record> data) {
        if (isReset()) {
            return;
        }

        mRecords = data;

        if (isStarted()) {
            super.deliverResult(data);
        }
    }

    @Override
    protected void onStopLoading() {
        super.onStopLoading();
        cancelLoad();
    }

    @Override
    protected void onReset() {
        super.onReset();
        clearResources();
    }

    private void clearResources() {
        mRecords = null;
    }
}
